let numberOfElements = 0
let numberOfChecked = 0


function addToLocalStorage(todoElement){
    let todo = JSON.parse(localStorage.getItem('todo'))
    if(todo == null){
        todo = []
    }
    todo.push(todoElement)
    localStorage.setItem('todo', JSON.stringify(todo))
}

function loadChecklist(){
    let todo = JSON.parse(localStorage.getItem('todo'))
    return todo
}

function addChecklistToHtml(todo){
    if(todo == null){
        console.log('your checklist is empty, add someting to your checklist')
    }
    else{
        console.log(todo)
        let length = todo.length
        for(i = 0; i<length; i++){
            let element = '<li id = "single-element"><label class="container" >'+todo[i].value+'<input type="checkbox" name = "checkbox" id = '+todo[i].value+' class = "element" ><span class="checkmark"></span></label></li>'
            $('#list-of-elements').prepend(element);
        }
    }
}

function clearlocalStorage(){
    let empty = []
    localStorage.setItem('todo', JSON.stringify(empty))
}


$(document).ready(function(){
    addChecklistToHtml(loadChecklist())
    $('#submit-button').click(function(){
        console.log('hey our event listener is working')
        let dict = {
            value : $('#box').val(),
            state : 1
        }
        $('#box').val('')
        // todo.push(dict)
        addToLocalStorage(dict)
        console.log(dict)
        let element = '<li id = "single-element"><label class="container" >'+dict.value+'<input type="checkbox" name = "checkbox" id = '+dict.value+' class = "element"><span class="checkmark"></span></label></li>'
        $('#list-of-elements').prepend(element);
        numberOfElements = numberOfElements + 1
    })  

    $('#clear-button').click(function(){
        clearlocalStorage()
        location.reload()
    })

    $('.element').change(function(){
        console.log('checked the element')
        console.log($('.element').attr('id'))
    })
})  












